package pa1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

public class SessionThread extends Thread {

	private final static int MAX_NAME_LENGTH = 100;
	
	private Socket socket;     //the server socket
	private Logger logger;     //the server's log of chat messages

	private User user;		   //the user associated with this particular session
	private String username,   //the client's username
	               color;      //what color the client's name will appear in chat
	private ArrayList<User> privateUsers = new ArrayList<>();  //list of users this client is private messaging. if empty, user's messages will be public
	
	BufferedReader fromClient;
	PrintWriter toClient;

	public SessionThread(Socket s, Logger l)
	{
		socket = s;
		logger = l;
	}

	public void run() 
	{
		try {
			//set up a reader and a writer connected to the client service socket
			fromClient = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
			toClient = new PrintWriter( new OutputStreamWriter( socket.getOutputStream() ), true );
			
			boolean loggedIn = false;
			
			//wait for an "@name" command from client and attempt to log in
			while(!loggedIn){
				String message = fromClient.readLine();
				
				//if the user wants to exit, shut it all down
				if(message.trim().equals("@exit")){
					socket.close();
					return;
				}
				
				if(message.startsWith("@name ")){
					username = message.substring("@name ".length());
					username = username.trim();
					
					loggedIn = login(username);
				}
				
				//if the client sent anything that did not start with "@name" or "@exit", try again
				else{
					toClient.println("Please log in with @name [your-username]");
				}
			}

			System.out.println(username + " has signed in.");
			broadcast(username + " has signed in.");
			
			//print chat history
			ArrayList<String> history = logger.read();
			
			for(int i = 0; i < history.size(); i++){
				toClient.println(history.get(i));
			}
			
			String input;
			String formattedName = "\033[1;" + color + "m" + username + "\033[0m: ";
			
			//wait for client to send messages
			while ( (input = fromClient.readLine()) != null ){				
				
				//if the message starts with "@", process the command
				if(input.startsWith("@")){
					parseCommand(input);
				}
				
				//if the message does not start with "@" send it to the appropriate set of users
				else{
					synchronized(logger.getCurrentUsers()){
						//if private communication is off, broadcast to everyone and log the message
						if(privateUsers.size() == 0){
							broadcast(formattedName + input);
							logger.write(formattedName + input);
						}
						//if private communication is on, private broadcast
						else{
							privateBroadcast(formattedName + input);
						}
					}
				}
			}
			
			//if the while loop ends, then the client's socket has closed. Log off user and end thread.
			System.out.println(username + " has signed off. No signal.");
			logoff();
			socket.close();
			return;
		}
		//This exception is thrown if the client unexpectedly quits.
		catch (SocketException e)
		{
			System.out.println(username + " has signed off. Socket exception.");
			logoff();
			return;
		}
		catch(IOException e){
			e.printStackTrace();
			System.out.println("IO error.");
		}
	}
	
	public boolean login(String username){
		
		//check if the username meets length requirements
		if(username.length() >= MAX_NAME_LENGTH || username.length() == 0){
			toClient.println("Username must be between 1-100 characters");
			return false;
		}
		
		//attempt to log in
		int loginCode = logger.login(username, socket);
			
		//if logger.login returns null, then user account was not created
		if(loginCode == Logger.OKAY){
			toClient.println("Welcome to chat, " + username);
			user = logger.getUser(username);
			color = user.getColor();
			return true;
		}
		else if(loginCode == Logger.CHAT_FULL){
			toClient.println("Chat room full, please try again later.");
			return false;
		}
		else if(loginCode == Logger.SAME_NAME){
			toClient.println("That user already exists, please try another name.");
			return false;
		}
		else{
			return false;
		}
	}
	
	public void logoff() {
		broadcast(username + " has signed off.");
		
		//free up all target users
		for(User u: privateUsers){
			u.setPrivate(false);
		}
		
		logger.logoff(user);
	}
	
	//this method processes the commands "@exit", "@who", "@private", "@end"
	public void parseCommand(String input){
		
		try{
			if(input.trim().equals("@exit")){
				System.out.println(username + " has signed off. Client disconnected.");
				logoff();
				
				toClient.println("@logout");
				socket.close();
				return;
			}
			
			else if(input.trim().equals("@who")){
				String userString = "current users: ";
				
				synchronized(logger.getCurrentUsers()){
					for(User u: logger.getCurrentUsers()){
						
						if(u.getPrivate() == true){
							userString += u.getName() + " (in private chat),";
						}
						else{
							userString += u.getName() + ",";
						}
					}
				}

				toClient.println(userString.substring(0, userString.length()-1));
			}
			
			else if(input.startsWith("@private ")){
				String targetName = input.substring("@private ".length());
				if(targetName.equalsIgnoreCase(username)){
					toClient.println("That's you!");
				}
				else{
					ArrayList<User> currentUsers;
					synchronized(currentUsers = logger.getCurrentUsers()){
						
						for(User target: currentUsers){
							if(targetName.equalsIgnoreCase(target.getName())){
								if(target.getPrivate() == false){
									toClient.println("Starting private chat with " + target.getName());
									target.setPrivate(true);
									privateUsers.add(target);
									return;
								}
								else{
									toClient.println("Target user is already in private communication with another user.");
									return;
								}
							}
						}
						toClient.println("Target user not currently logged on.");
					}
				}
			}
			
			else if(input.startsWith("@end ")){
				String targetName = input.substring("@end ".length());
				if(targetName.equalsIgnoreCase(username)){
					toClient.println("That's you!");
				}
				else{
					for(User target: privateUsers){
						if(targetName.equalsIgnoreCase(target.getName())){							
							
							boolean tryRemove = privateUsers.remove(target);
							
							if(!tryRemove){
								toClient.println("You are not in private chat with " + target.getName());
								return;
							}
							else{
								toClient.println("Ending private chat with " + target.getName());
								target.setPrivate(false);
								return;
							}
						}
					}
					toClient.println("You are not in private chat with " + targetName);
				}
			}
			
			else if(input.startsWith("@name ")){
				toClient.println("You're already logged in as " + username);
			}
			
			else{
				toClient.println("Command not recognized. Valid commands: @exit, @who, @private [username], @end [username]");
			}	
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	//this method sends the client's message to every user
	public void broadcast(String message){
		
		try{
			for(User peer: logger.getCurrentUsers()){
				PrintWriter broadcaster = new PrintWriter( 
							new OutputStreamWriter(peer.getSocket().getOutputStream()), true );
				broadcaster.println(message);
				}	
			}
		catch(IOException e){
			e.printStackTrace();
		}	
	}
	
	//this method sends a message to every client on the user's list of private communication.
	//if the target user has since logged off, they are removed from the list.
	public void privateBroadcast(String message){
		try{
			int numUsers = privateUsers.size();
			for(int i = 0; i < numUsers; i++){
				User peer = privateUsers.get(i);
				
				//if the target user's socket is null or closed, remove them from the list.
				if(peer.getSocket() == null || peer.getSocket().isClosed()){
					privateUsers.remove(i);
					i--;
					numUsers = privateUsers.size();
				}
				else{
					PrintWriter broadcaster = new PrintWriter( 
							new OutputStreamWriter(peer.getSocket().getOutputStream()), true );
					broadcaster.println("(private) " + message);
				}
			}
			
			//if it turns out every user has disconnected, public broadcast the message instead
			if(privateUsers.size() == 0){
				broadcast(message);
				logger.write(message);
			}
		}
		//this exception is thrown if the target user has signed off
		catch(SocketException e){
			System.out.println("Target of private communication has logged off.");
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}