package pa1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

public class ClientThread extends Thread {

	private static final int READ = 0, WRITE = 1;
	private int role;
	private Socket socket;     //the client socket

	public ClientThread(Socket socket, int role)
	{
		this.socket = socket;
		this.role = role;
	}

	//depending on what was selected, run the read loop or the write loop
	public void run()
	{
		if(role == READ){
			read();
		}
		
		else if(role == WRITE){
			write();
		}
	}
	
	//accepts messages from the server and writes them to the console
	public void read(){
		
		BufferedReader fromServer;
		
		try{
			fromServer = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
			
			String serverMessage;
			while( (serverMessage = fromServer.readLine()) != null){
				
				//if the server sends a logout message, end the thread
				if(serverMessage.equals("@logout")){
					fromServer.close();
					socket.close();
					System.out.println("Client side socket closed.");
					return;
				}
				System.out.println(serverMessage);
			}
			
		}
		catch(SocketException e){
			System.out.println("Server not found, closing client.");
			System.exit(0);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//accepts messages from the system and sends them to the server
	public void write(){
		PrintWriter toServer;
		BufferedReader stdIn = new BufferedReader( new InputStreamReader( System.in ) );		
		
		try{
			toServer = new PrintWriter( new OutputStreamWriter( socket.getOutputStream() ), true );
			
			String chatMessage;
			while( (chatMessage = stdIn.readLine()) != null){
				
				//if the user type @exit, relay message, then end the write thread
				if(chatMessage.equals("@exit")){
					toServer.println(chatMessage);
					return;
				}
				toServer.println(chatMessage);
			}	
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}