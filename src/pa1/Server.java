package pa1;

import java.net.ServerSocket;
import java.net.Socket;

//This class creates a ServerSocket and sets it to listen to connections from clients
public class Server {
	
	final static int PORT = 3000;
	
	public static void main( String [] arg ) throws Exception
	{
		ServerSocket serverSocket = new ServerSocket( PORT, 20 );
		Socket socket;
		Logger logger = new Logger();
		
		serverSocket.setReuseAddress( true );
		System.out.println("Server running and accepting connections.");
		
		//loops, waiting for incoming connections
		while ( (socket = serverSocket.accept()) != null ){
			System.out.println( "Accepted an incoming connection." );
			new SessionThread(socket, logger).start();
		}
		serverSocket.close();
	}
}