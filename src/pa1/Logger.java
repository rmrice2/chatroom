package pa1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class Logger {
	
	static final int CHAT_FULL = 0, SAME_NAME = 1, OKAY = 2;  //status codes for use by SessionThread
	
	private String historyfile = "log.txt",  //history file stores user messages
				   userfile = "users.txt";  //user file stores registered user names and colors
	private ArrayList<User> userList;       //holds all registered users
	private ArrayList<User> currentUsers;   //holds all current users. Used as a monitor object by SessionThread.
	
	public Logger(){
		userList = new ArrayList<>();
		currentUsers = new ArrayList<>();
		
		//create user objects from users.txt
		try{
			BufferedReader br = new BufferedReader(new FileReader(userfile));
			String line;
			
			while( (line = br.readLine()) != null){
				String[] tokens = line.split("\t");
				
				//tokens[0] is username, tokens[1] is color
				userList.add(new User(tokens[0], tokens[1]));
			}
			br.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	//this thread takes the given String and socket, checks if a user with that name already exists
	//possible race conditions: 2 users with the same name sign on at the same time, 2 users sign on when 
	//client count is at 19.
	public int login(String username, Socket socket){
		
		synchronized(currentUsers){
			
			//login rejected if chat room is full
			if(currentUsers.size() >= 20){
				return CHAT_FULL;
			}
			
			//login rejected if another client already has the username
			for(int i = 0; i < currentUsers.size(); i++){
				if(username.equalsIgnoreCase (currentUsers.get(i).getName())){
					return SAME_NAME;
				}
			}
			
			//login accepted, account already exists, no need to assign a new color
			for(User account: userList){
				if(username.equalsIgnoreCase(account.getName())){
					account.setSocket(socket);
					currentUsers.add(account);
					return OKAY;
				}
			}
			
			//login accepted, new account, assign a new color
			Random rand = new Random();
			int rannum = rand.nextInt(7) + 30;
			
			User newbie = new User(username, Integer.toString(rannum));
			newbie.setSocket(socket);
			userList.add(newbie);
			currentUsers.add(newbie);	
			
			//write the new user + color to file
			try{
				PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(userfile, true)));
				pw.write(newbie.getName() + "\t" + newbie.getColor() +"\n");
				
				pw.close();
				
			} catch(IOException e){
				e.printStackTrace();
			}
			
			return OKAY;
		}
	}
	
	//resets the user and removes them from currentUsers
	public void logoff(User user){
		
		synchronized(currentUsers){
			user.reset();
			currentUsers.remove(user);	
		}
	}
	
	public ArrayList<User> getCurrentUsers(){
		
		synchronized(currentUsers){
			return currentUsers;
		}
	}
	
	//gets user by name
	public User getUser(String username){
		
		synchronized(currentUsers){
			for(User u: currentUsers){
				if(username.equalsIgnoreCase(u.getName())){
					return u;
				}
			}
			return null;
		}
	}
	
	//returns an ArrayList containing every line from the chat log
	public synchronized ArrayList<String> read(){
		ArrayList<String> lines = new ArrayList<>();
		String line;
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(historyfile));
			while( (line = br.readLine()) != null){
				lines.add(line);
			}
			
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("log.txt not found");
		} catch (IOException e) {
			System.out.println("File error");
			e.printStackTrace();
		}
		
		return lines;
	}
	
	//accepts a String and writes it to the chat log
	public synchronized void write(String line){
		try{
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(historyfile, true)));
			pw.write(line + "\n");
			
			pw.close();
			
		} catch(IOException e){
			e.printStackTrace();
		}
	}
	
}
