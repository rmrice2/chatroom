package pa1;

import java.net.Socket;

public class User {

	private String name, color;
	private Socket socket;
	private boolean inPrivate;
	
	public User(String name, String color){
		this.name = name;
		this.color = color;
	}
	
	//reserves this user as a target of private chat. synchronized so that two users cannot target the same user
	public synchronized void setPrivate(boolean inPrivate){
		this.inPrivate = inPrivate;
	}
	
	public void setSocket(Socket socket){
		this.socket = socket;
	}
	
	public Socket getSocket(){
		return socket;
	}
	
	public String getName(){
		return name;
	}
	
	public String getColor(){
		return color;
	}
	
	public synchronized boolean getPrivate(){
		return inPrivate;
	}
	
	//used when the user logs off, blanks out session specific fields
	public void reset(){
		socket = null;
		inPrivate = false;
	}

}
