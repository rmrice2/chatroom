package pa1;

import java.net.ConnectException;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

//This class creates a client that attempts to sign on to a remote server.
//Once it has signed on, it sends messages to the server and receives all 
//public messages sent by other users to the server.

public class Client {

	private static final int READ = 0, WRITE = 1; //
	
	public static void main( String [] arg ) throws Exception
	{
		Socket socket;
		boolean connected = false;
		
		//loop until server is found
		while(!connected){
			try{
				socket = new Socket( arg[0], 3000 ); //this line throws an exception if server is not found
				connected = true;                    //otherwise, if the server was found, the loop will end
				
				//start one thread for reading messages from the server and another for sending them to the server
				new ClientThread(socket, READ).start();
				new ClientThread(socket, WRITE).start();
			}
			
			catch(ConnectException e){
				System.out.println("Server not found, will retry connection in 3 seconds.");
				TimeUnit.SECONDS.sleep(3);
			}	
		}
		
		System.out.println("Welcome to CS352 chat. To log in, type @name [your-username].");
	}
}